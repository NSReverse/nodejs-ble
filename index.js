const { exec } = require('child_process');
const bleno = require('bleno');
var prefix = 'MATRIX-';
var bindingPhrase = '8yu938w98eryq983yen9q28yeu';
var pairedClient = '';

bleno.on('stateChange', function(state) {
    console.log('stateChange: ' + state);

    if (state === 'poweredOn' && pairedClient === '') {
        bleno.startAdvertising(`${prefix}${bleno.address.replace(/:/g, '').toUpperCase()}`, ['1803']);
    }
    else {
        bleno.stopAdvertising();
    }
});

bleno.on('accept', function(clientAddress) {
    console.log('accept: ' + clientAddress);

    /*
    // remove connected client
    if (pairedClient !== '') {
        bleno.disconnect(clientAddress);
        console.log('Disconnected ' + clientAddress + ' due to reason: unauthorized');
    }
    */

    if (pairedClient !== '') {
        // Client has come back
        console.log('    - client reconnected, stopping advertisement');
        bleno.stopAdvertising();
    }

    bleno.updateRssi();
});

bleno.on('disconnect', function(clientAddress) {
    console.log('disconnect: ' + clientAddress);

    if (pairedClient !== '') {
        console.log('    - lost paired client unexpectedly, beginning advertisement');
        bleno.startAdvertising(`${prefix}${bleno.address.replace(/:/g, '').toUpperCase()}`, ['1803']);
    }
});

bleno.on('rssiUpdate', function(rssi) {
    console.log('rssiUpdate: ' + rssi);
});

bleno.on('advertisingStart', function(error) {
    console.log('advertisingStart: ' + (error ? 'error ' + error : 'success'));

    if (!error) {
        bleno.setServices([
            new bleno.PrimaryService({
                uuid: '12ab',
                characteristics: [
                    new bleno.Characteristic({
                        value: null,
                        uuid: '34cd',
                        properties: ['notify', 'read', 'write'],

                        onSubscribe: function(maxValueSize, updateValueCallback) {
                            console.log('Device Subscribed');
                            this.intervalId = setInterval(function() {
                                console.log("Send hello");
                                updateValueCallback(new Buffer("Hello"));
                            }, 1000);
                        },

                        onUnsubscribe: function() {
                            console.log('Device unsubscribed');
                            clearInterval(this.intervalId);
                        },

                        onReadRequest: function(offset, callback) {
                            console.log('Read request received');
                            clearInterval(this.intervalId);
                        },

                        onWriteRequest: function(data, offset, withoutResponse, callback) {
                            this.value = JSON.parse(data.toString('utf-8'));
                            console.log('Write request with value: ' + data.toString('utf-8'));
                            callback(this.RESULT_SUCCESS);

                            if (typeof (this.value.client) !== 'undefined') {
                                if (pairedClient === '') {
                                    // we do not have a paired device, bind to this client
                                    var clientCandidate = this.value.client;
                                    var passphraseCandidate = this.value.passphrase;

                                    if (bindingPhrase === passphraseCandidate && this.value.data === 'bind') {
                                        pairedClient = clientCandidate;
                                        console.log('Client binding successful, stopping advertisement.');
                                        bleno.stopAdvertising();
                                    }
                                    else {
                                        console.log('Request failed, authentication failed or not provided.');
                                    }
                                }
                                else {
                                    if (pairedClient === this.value.client) {
                                        // Paired client making a request
                                        let btData = this.value.data;

                                        if (btData.includes('shell')) {
                                            exec(this.value.data.replace('shell', '').trim(), (error, stdout, stderr) => {
                                                console.log(stdout);
                                            });
                                        }
                                        else if (btData.includes('show-paired-client')) {
                                            console.log('Paired Client: ' + pairedClient);
                                        }
                                    }
                                    else {
                                        console.log('Write request rejected. Requestor not paired client');
                                    }
                                }
                            }
                            else {
                                console.log('Write request rejected. Invalid request format.');
                            }
                        }
                    })
                ]
            })
        ]);
    }
});

bleno.on('advertisingStop', function() {
    console.log('advertisingStop');
});


bleno.on('servicesSet', function(error) {
    console.log('servicesSet: ' + (error ? 'error ' + error : 'success'));
});
